### Keybase proof

I hereby claim:

  * I am eysz7x on github.
  * I am eysz7x (https://keybase.io/eysz7x) on keybase.
  * I have a public key ASBgn5PRgbz3xgX6lcQ0uE8rstxyU-TIGYfRNaLaR7x2rgo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "010121bb853acb7f914aad471bed2e664c783da583ef6250f4ff574509ed8468a8810a",
      "host": "keybase.io",
      "kid": "0120609f93d181bcf7c605fa95c434b84f2bb2dc7253e4c81987d135a2da47bc76ae0a",
      "uid": "85c1ba34cfe4e54b726b6c85d7a99119",
      "username": "eysz7x"
    },
    "merkle_root": {
      "ctime": 1538213618,
      "hash": "8aff1556108e50e642aa64d456bdf58e7cdb4429370d2d0a6b4fd01f8e3d0e30065de8938ed1039feb3e1d79590f2c2b7c8ce9c18a9164ee816b4df6d7adf5e5",
      "hash_meta": "d57af0ac0895a87b476cce793c2a79a2a49edba1b0af0c3baee304687622b80d",
      "seqno": 3710983
    },
    "service": {
      "entropy": "uKM+hROtKH2c02+b22DRYty/",
      "name": "github",
      "username": "eysz7x"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.6.2"
  },
  "ctime": 1538213660,
  "expire_in": 504576000,
  "prev": "dbc1130539cb35bca81549d5c6daeeba92843214c3c55116466a9db7bfcb6029",
  "seqno": 30,
  "tag": "signature"
}
```

with the key [ASBgn5PRgbz3xgX6lcQ0uE8rstxyU-TIGYfRNaLaR7x2rgo](https://keybase.io/eysz7x), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgYJ+T0YG898YF+pXENLhPK7LcclPkyBmH0TWi2ke8dq4Kp3BheWxvYWTESpcCHsQg28ETBTnLNbyoFUnVxtruupKEMhTDxVEWRmqdt7/LYCnEIOAUuFzlja4pHrmbh2nz+89FQ7H6u8iMZ2b1ufmcUZrPAgHCo3NpZ8RAfrEiyW2wox3QY8XEdzaMXrroQPamigXiinotRHUyo8R3eSv+40TopqNVNKiK6ucF1bElxg1criKNmLlau2AhCKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIGZJpurgF6CNvnjzlqR71fLfxcAtulN28Oeby6s+NFd/o3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/eysz7x

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id eysz7x
```